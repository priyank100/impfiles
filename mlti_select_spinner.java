public class MultiSelectionSpinner extends AppCompatSpinner implements DialogInterface.OnMultiChoiceClickListener {
    ArrayList<Model> items = null;
    boolean[] selection = null;
    ArrayAdapter adapter;

    public MultiSelectionSpinner(Context context) {
        super(context);
        adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        super.setAdapter(adapter);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        super.setAdapter(adapter);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (selection != null && which < selection.length) {
            selection[which] = isChecked;
            adapter.clear();
//            adapter.add(buildSelectedItemString());
            if (getSelectedItems().size() == 0) {
                adapter.add("Select Recipients");
            } else if (items.size() == getSelectedItems().size()) {
                adapter.add("All Selected");
            } else {
                adapter.add(getSelectedItems().size() + " Recipient Selected");
            }

        } else {
            throw new IllegalArgumentException("Argument 'which' is out of bounds.");
        }
    }

    @Override
    public boolean performClick() {
        if (items.size() != 0) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            String[] itemNames = new String[items.size()];

            for (int i = 0; i < items.size(); i++) {
                itemNames[i] = items.get(i).getLoginId();
            }
            builder.setMultiChoiceItems(itemNames, selection, this);

            builder.setNeutralButton("Select All", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setSelection(items);
                }
            });
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    // Do nothing
                }
            });
            builder.show();
            return true;
        } else {
            Constant.Toast(getContext(), "No data found !");
            return false;
        }
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(ArrayList<Model> items) {
        this.items = items;
        selection = new boolean[this.items.size()];
        adapter.clear();
        adapter.add("Select Recipients");
        Arrays.fill(selection, false);
    }

    public void setSelection(ArrayList<Model> selection) {
        for (int i = 0; i < this.selection.length; i++) {
            this.selection[i] = false;
        }

        for (Model sel : selection) {
            for (int j = 0; j < items.size(); ++j) {
                if (items.get(j).getDeviceId().equals(sel.getDeviceId())) {
                    this.selection[j] = true;
                }
            }
        }
        adapter.clear();
//        adapter.add(buildSelectedItemString());
        adapter.add("All Selected");
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(items.get(i).getLoginId());
            }
        }
        return sb.toString();
    }

    public ArrayList<Model> getSelectedItems() {
        ArrayList<Model> selectedItems = new ArrayList<>();

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                selectedItems.add(items.get(i));
            }
        }
        return selectedItems;
    }

    public JSONArray getSelectedDeviceIds() {
        JSONArray selectedDevice = new JSONArray();
            for (int i = 0; i < items.size(); ++i) {
                if (selection[i]) {
                    selectedDevice.put(items.get(i).getDeviceId());
                }
            }
        return selectedDevice;
    }
}