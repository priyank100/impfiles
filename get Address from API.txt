public void getAddress() {
        final ArrayList addressList = new ArrayList();
        gps = new GPSTracker(getContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            progressDialog.show();

            System.out.println("lat & long>>" + latitude + ">>" + longitude);

            String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + googleLocationAPIKey;
            System.out.println("location url>>" + URL);

            VolleyRequest.getInstance(getContext()).ServiceCall(URL, new InstantData<String>() {
                @Override
                public void getResponse(String response) {
                    progressDialog.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.isNull("status") ? "" : jsonObject.getString("status");
                        System.out.println("Status>> "+ status);

                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String fullAddress = object.isNull("formatted_address") ? "" : object.getString("formatted_address");
                            addressList.add(fullAddress);
                        }
                        System.out.println("address list >> " + addressList.toString());

                        if (addressList.size() > 0) {
                            if (!addressList.get(0).toString().isEmpty()) {
                                addressStr = addressList.get(0).toString();
                                System.out.println("address>> " + addressStr);

                            } else {
                                addressStr = addressList.get(1).toString();
                                System.out.println("address>> " + addressStr);
                            }
                        } else {
                            addressStr = "No Address Found";
                            Toast.makeText(getContext(), "No Address Found", Toast.LENGTH_SHORT).show();
                            System.out.println("address>> " + addressStr);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void getResponseError(String error) {
                }
            });

        } else {
            flagGPS = 0;
            gps.showSettingsAlert(new InterFaceClass<String>() {

                @Override
                public void getResponse(String repose) {
                    if (repose.equals("On")) {
                        getAddress();
                    }
                }
            });
        }
    }