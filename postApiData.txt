public void postData(String url) {
        progressDialog.show();

        final String pinCode = pincodeText.getText().toString().trim();
        Log.d("post url>>>", url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response>>>", response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (status.equalsIgnoreCase("True")) {
                        checkText.setText(pinCode+" - This pincode is under serviceable.");
                        pincodeText.setText("");
                    }
                    else {
                        checkText.setText(pinCode+" - "+message);
                        pincodeText.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Volley error >>>", error.getMessage());
                progressDialog.dismiss();
            }
        })

        {

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                headers.put("Authorization","auth-key");
                return headers;
            }*/


            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("Pincode", pinCode);
                    String requestBody = jsonBody.toString();

                    return requestBody == null ? null : requestBody.getBytes("utf-8");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }