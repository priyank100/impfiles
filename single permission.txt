RunTime Permission :--

1. private static final int PERMISSIONS_REQUEST_CAMERA = 123;


2. public boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(RequestIdPage.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }


3. private void requestPermission() {
        ActivityCompat.requestPermissions(RequestIdPage.this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
    }


4. onCreate -- 
		if (checkPermission()) {
                            openDialog();
                        }
                        else {
                            requestPermission();
                        }

5. Handle permission -- 
	in override method - onRequestPermissionsResult - 

		if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (ContextCompat.checkSelfPermission(RequestIdPage.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                openDialog();

            } else {
                requestPermission();
            }
        }