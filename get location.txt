<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.INTERNET" />


private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = android.Manifest.permission.ACCESS_FINE_LOCATION;
    String corusPermission = Manifest.permission.ACCESS_COARSE_LOCATION;
    GPSTracker gps;
    String addressStr = "";
    int flagGPS = 0;
    double latitude, longitude;


public void gpsActivity() {
        textView1.setText("Scanning...");
        toolbar_text.setText("GPS Test");
        imageViewMicro.setImageResource(R.drawable.gps);
        setImageViewInScrollViewFirstGPS();

        try {
            if (ActivityCompat.checkSelfPermission(BluethootActivity.this, mPermission) != MockPackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BluethootActivity.this, new String[]{mPermission}, REQUEST_CODE_PERMISSION);
                return;
            } else {
                callGPSCheck();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

public void callGPSCheck() {
        gps = new GPSTracker(BluethootActivity.this);
        if (gps.canGetLocation()) {
            List<Address> list = null;
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            Geocoder myLocation = new Geocoder(this, Locale.getDefault());

            try {
                list = myLocation.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (list != null && list.size() != 0) {
                Address address = list.get(0);
                addressStr += address.getAddressLine(0) + " ";
                textView1.setText("Location::" + addressStr);
                Log.d("location>>>", addressStr);
            } else {
                textView1.setText("GPS Enabled");
                Log.d("location>>>", "no location");
            }
            flagGPS = 1;
            startAnimationGPS();

        } else {
            flagGPS = 0;
            gps.showSettingsAlert(new InterFaceClass<String>() {
                @Override
                public void getResponse(String repose) {
                    if (repose.equals("On")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // startAnimationGPS();
                                callGPSCheck();
                            }
                        }, 4000);
                    }
                }
            });
            // startAnimationGPS();
        }
    }