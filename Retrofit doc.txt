Retrofit dependencies -- 

    // retrofit
    implementation "com.squareup.retrofit2:retrofit:2.3.0"
    implementation "com.squareup.retrofit2:adapter-rxjava2:2.3.0"
    implementation "com.squareup.retrofit2:converter-gson:2.3.0"

    // rxandroid
    implementation "io.reactivex.rxjava2:rxandroid:2.0.1"
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.1'
    implementation 'com.squareup.okhttp3:okhttp:3.4.1'


--> Create Model-Class with SerializableName or using POJO(Plain Old Java Objects) 
    and ApiServiceFactory.java class 


--> public class ApiServiceFactory {

   private static ApiService apiService_instance;

   public static ApiService getApiService() {

        if (apiService_instance == null) {
            synchronized (ApiServiceFactory.class) {
                if (apiService_instance == null) {
                    apiService_instance = getRetrofit().create(ApiService.class);
                }
            }
        }
        return apiService_instance;
    }


    public final Retrofit getRetrofit(@NotNull String baseUrl) {
      Intrinsics.checkParameterIsNotNull(baseUrl, "baseUrl");
      Retrofit retrofit = (Retrofit)null;
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(Level.BODY);
      OkHttpClient client = (new Builder()).addInterceptor((Interceptor)interceptor).connectTimeout(1000L, TimeUnit.SECONDS).readTimeout(1000L, TimeUnit.SECONDS).build();
      retrofit = (new retrofit2.Retrofit.Builder()).client(client).baseUrl(baseUrl).addConverterFactory((Factory)GsonConverterFactory.create()).build();
      return retrofit;
   }


    public interface ApiService {
	// Get
	@GET("qcloginemp.aspx?")
      	Call getLogin(@Query("emp_id") @NotNull String var1, @Query("password") @NotNull String var2);

        // Post
        @FormUrlEncoded
        @POST(BasePosition.API_LOGIN)
        Call<ResponseBody> getLoginUserIdentify(@Field("username") String userName, @Field("password") String passWord));
    }
}


--> Inside MainActivity.java class

	ApiServiceFactory.ApiService apiService = ApiServiceFactory.getApiService();

        apiService.getLoginUserIdentify(userName, passWord, "password").enqueue(new Callback<ResponseBody>() {

            @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

   	//ur operation

    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        dialog.dismiss();
      //
    }
});