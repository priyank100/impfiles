Retrofit Example--

Get API-

1. Create Interface ApiInterface.java-
	public interface ApiInterface 
	{
    		@GET("/last part of main url/")	//everything?sources=the-times-of-india&apiKey=/*key*/
    		Call<ModelClass> getModelClass(); or Call<List<ModelClass>> getModelArticle();
	}

2. Create Model-Class with SerializableName or using POJO(Plain Old Java Objects)

3. Main class-

	public void getDataUsingRetrofit() 
	{
        	String baseUrl = "main-url";	//https://newsapi.org/v2/
        	Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();

        	ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        	Call<ModelClass> call = apiInterface.getModelClass();

        	call.enqueue(new Callback<ModelClass>() {
            	@Override
            	public void onResponse(Call<ModelClass> call, retrofit2.Response<ModelClass> response) {
                	Log.d("retrofit response >>>", response.body().getArrayName()+"");
            	}

            	@Override
            	public void onFailure(Call<ModelClass> call, Throwable t) {
                	Log.d("Retrofit failure >>>", t.getMessage());
            	}
       	 	});
	}